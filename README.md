# JavaScript React GraphQL
Helper project that consumes a GraphQL API with React.

![](./docs/image.png)

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Start dev server: `npm start`
- Ensure the [GraphQL API](https://gitlab.com/mburolla/javascript-graphql) is running

# React GraphQL Setup
- [index](./src/index.js)

# GraphQL Consumption
- [Users](./src/Users.js)

# Links
- [GraphQL React Clients](https://www.freecodecamp.org/news/5-ways-to-fetch-data-react-graphql/)
