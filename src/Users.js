import React from 'react'
import { useQuery } from 'urql'

export const Users = ({id}) => {
    const [result] = useQuery({
        query: `
        {
            getUser(id: ${id}) {
                id
                first_name
                last_name
            }
        }
        `
    })
    const { data, fetching, error } = result
    if (fetching) return "Loading...";
    if (error) return <pre>{error.message}</pre>

    return (
        <div className='Users'>
        <h1>User Information</h1>
        Id: {data.getUser.id} <br />
        First Name: {data.getUser.first_name} <br />
        Last Name: {data.getUser.last_name} <br />
      </div>
    )
}
