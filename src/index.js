import App from './App';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { createClient, Provider } from 'urql'; // <== React GraphQL Client.

const client = createClient({ 
  url: 'http://localhost:5150/graphql/',
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider value={client}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider> 
);
